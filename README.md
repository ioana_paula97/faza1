

<<<<<FISIERUL TXT ESTE LA SOURCE>>>>>>>>>>

PROIECT - FAZA1:
TEMA 11- Manager de artisti favoriţi integrat cu DeviantArt
Echipa: Echipa Traila si Stoian grupa 1075 - proiect mic
--STOIAN IOANA PAULA
--TRAILA IULIA IOANA

FUNCTIONALITATI:
Obs: API-ul DeviantArt imi permite sa accesez si sa manipulez elementele favorite si colectiile unui anumit utilizator.
-Preluare continut din categoria favorite corespunzatoare unui anumit cont de pe DeviantArt si afisarea intr-o lista: artist, deviation (desen/pictura/creatie artistica), numele deviation.
-Posibilitatea de a sorta lista dupa anumite criterii: nume asrtist, nume deviation, data postarii.
-Posibilitatea de a afisa continutul unei colectii de pe DeviantArt
-Posibilitatea de a adauga sau sterge elemente din categoria favorite sau din colectii.


MODULE
-Front end: pagina web contine o lista cu elementele afisate si butoane pentru sortare, filtrare dupa anumite criterii, adaugare si stergere elemente.
-Back-end: acceseaza date din serviciul extern DeviantArt si baza de date MySQL.
-Baza de date:
--Enititati: Conturi (nume, id_cont), Artisti (nume, id_artist), Deviations (id_deviation, titlu, imagine, data postarii, id_artist) Colectii(id_colectie, nume_colectie, id_cont, id_deviation).


DIAGRAMA BAZA DE DATE
Downloads Bitbucket
https://bitbucket.org/ioana_paula97/proiect_faza1/downloads/diagrama_bd.png

Google Drive
https://drive.google.com/open?id=1mlsDCUkRu8CI7wpRKUYmvyMp4Vyb9QkM
